const { hover } = require('@testing-library/user-event/dist/hover');

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
   
      
    extend: {
      colors: {
        'green1': '#4eb14e ',
        'gray':' #F3F4F6',
        'gray1':' #51484f ',
       'gray2':'#696969', 
       'gray3':'#717171',
       'gray4':'#89939E',
       'gray5':' #F5F7FA ',
       'gray6':'#d3d3d3',
       'black1':'#263238',
       'white1':'#F5F7FA',
       'white2':'#ffffff '
       
      },
      fontFamily: {
        roboto: ['Roboto', 'sans-serif'],
        montserrat: ['Montserrat', 'sans-serif'],
        sans:['Open Sans', 'sans-serif'],
        sans1: ['Inter', 'sans-serif']
        },
        boxShadow: {
          bottom: ' 0px 2px 3px -1px rgba(201,201,201,0.76)',
          bottom1:'2px 10px 40px -10px rgba(138,134,134,0.75)'
        },
   
    },
  },
  plugins: [],
}

