function Calender() {
    return (
        <div className="flex flex-col lg:flex-row   bg-white mt-9 justify-center items-center px-5 lg:space-y-0 lg:px-0  lg:space-x-11 space-y-8">
            <div className="w-auto h-auto lg:w-[410px] lg:h-[420px] cursor-pointer " >
                <img src="pana.png"  alt="" />
            </div>
            
            <div className=" flex  flex-col items-start  justify-center     ">
                <h2 className="text-[33px]  font-semibold font-sans1 leading-tight  hover:text-green1 hover:underline  text-gray1 ">How to design your site footer like<br/>we did</h2>
                <p className="text-gray2 leading-snug text-[13px]  tracking-wide mt-4  ">Donec a eros justo. Fusce egestas tristique ultrices. Nam tempor, augue nec tincidunt <br/> molestie, massa nunc varius arcu, at scelerisque elit erat a magna. Donec quis erat at <br/> libero ultrices mollis. In hac habitasse platea dictumst. Vivamus vehicula leo dui, at porta<br/> nisi facilisis finibus. In euismod augue vitae nisi ultricies, non aliquet urna tincidunt. Integer<br/> in nisi eget nulla commodo faucibus efficitur quis massa. Praesent felis est, finibus et nisi<br/> ac, hendrerit venenatis libero. Donec consectetur faucibus ipsum id gravida.
                </p>
                <button className="rounded-md bg-green1 mt-7 hover:bg-green-500  text-white cursor-pointer border-2 flex justify-center items-center text-[15px] tracking-wide  py-3.5 px-7 ">Learn More</button>
            </div>
        </div>
     );
}
export default Calender