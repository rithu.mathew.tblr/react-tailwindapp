function Unlock() {
    return (
        <div className="flex flex-col lg:flex-row bg-white mt-8   py-2  px-5 sm:px-0 lg:space-x-10 justify-center items-center ">
            <div  className="w-auto h-auto xl:w-[400px]  xl:h-[400px] cursor-pointer">
                <img src="Frame 35.png"  alt="" />
            </div>
            
            <div className=" flex  flex-col items-start xl:py-[68px]    ">
                <h2 className="text-[33px]  font-semibold font-sans1 leading-tight cursor-pointer   text-gray1 hover:text-green1 hover:underline">The unseen of spending three<br/>years at Pixelgrade</h2>
                <p className="text-gray2 leading-snug text-[13px]  tracking-wide mt-4  ">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet justo ipsum. Sed <br/>accumsan quam vitae est varius fringilla. Pellentesque placerat vestibulum lorem sed <br/>porta. Nullam mattis tristique iaculis. Nullam pulvinar sit amet risus pretium auctor. Etiam <br/>quis massa pulvinar, aliquam quam vitae, tempus sem. Donec elementum pulvinar odio.
                </p>
                <button  className="rounded-md bg-green1 mt-7  text-white cursor-pointer border-2 flex justify-center items-center text-[15px] tracking-wide  py-3.5 px-7 hover:bg-green-500   ">Learn More</button>
            </div>
        </div>
     );
}
export default Unlock