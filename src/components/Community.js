function Community() {
    return (
        <div className="   bg-white  flex flex-col  xl:mt-8 ">
            <div className=" flex flex-col items-center ">
                <h2 className="text-[28px] xl:text-[32px]  font-semibold font-sans1 leading-tight  text-center text-gray1 ">Manage your entire community <br />in a single system</h2>
                <p className="text-gray2 text-sm mt-2 tracking-wide  hover:underline ">Who is Nextcent suitable for?
                </p>
            </div>
      
            <div className="flex flex-col md:flex-row justify-center py-4 mx-4 md:space-x-5 lg:space-x-9 xl:space-x-20 mt-6">
                <div className="group hover:bg-gray5     flex flex-col items-center  p-4 mb-6 md:mb-0 transform transition-transform hover:scale-110 rounded-md shadow-bottom">
                    <img src="Icon3.png" width="55" height="55" alt="" />
                    <h2 className="text-[27px] group-hover:text-green1 cursor-pointer  leading-tight tracking-tight font-bold font-sans1 text-center text-gray1 mt-4">Membership <br /> Organisations</h2>
                    <p className="text-gray2 text-[13px]  group-hover:text-black tracking-wide mt-2 text-center ">Our membership management<br /> software provides full automation of<br /> membership renewals and payments
                    </p>
                </div>
                <div className="group hover:bg-gray5    flex flex-col items-center mb-6 md:mb-0 rounded-md shadow-bottom p-4 transform transition-transform hover:scale-110">
                    <img src="Icon 2.png" alt="" width="55" height="55" />
                    <h2 className="text-[27px]  group-hover:text-green1 cursor-pointer  leading-tight tracking-tight font-bold font-sans1 text-center text-gray1 mt-4 ">National<br /> Associations</h2>
                    <p className="text-gray2  group-hover:text-black text-[13px] mt-2 tracking-wide text-center    ">Our membership management<br /> software provides full automation of<br /> membership renewals and <br />payments
                    </p>
                </div>
                <div className="group hover:bg-gray5    flex flex-col items-center rounded-md shadow-bottom p-4 transform transition-transform hover:scale-110">
                    <img src="Icon 1.png" alt="" width="55" height="55" />
                    <h2 className="text-[27px]  group-hover:text-green1 cursor-pointer  leading-tight tracking-tight font-bold font-sans1 text-center text-gray1 mt-4">Clubs And<br />Groups</h2>
                    <p className="text-gray2  group-hover:text-black text-[13px] mt-2 tracking-wide text-center   ">Our membership management <br />software provides full automation of<br /> membership renewals and payments
                    </p>
                </div>
            </div>
        </div>
    )
}
export default Community