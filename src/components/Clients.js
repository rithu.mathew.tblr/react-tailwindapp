function Client() {
    return (
       <div className=" bg-white px-24    mt-8">
        <div className=" flex flex-col items-center text-center">
        <h2 className="text-[27px] sm:text-[32px] md:text-[34px] font-semibold font-sans1 leading-tight text-gray1 ">Our Clients</h2>
               <p className="text-gray2 text-[13px] sm:text-sm mt-2 tracking-wide  ">We have been working with some Fortune 500+ clients</p>
        </div>
        <div className="flex flex-wrap  items-center justify-center space-x-5 sm:space-x-6 md:space-x-8 lg:space-x-14 space-y-2 xl:space-x-28  py-8 " >
        <img src="log1.png" className="" alt="" width="44" />
        <img src="log2.png" alt=""   width="44" />
        <img src="log6.png" alt=""  width="44"/>
        <img src="log3.png" alt="" width="44"  />
        <img src="log4.png" alt=""  width="44" />
        <img src="log5.png" alt=""  width="44" />
        <img src="log6.png" alt=""  width="44"/>
        </div>
       </div> );
    }
    export default Client
 