function Frame16() {
    return (
        <div className="flex mt-36 lg:mt-48 xl:mt-36 bg-gray flex-col justify-between text-center items-center   p-9 ">
           <h1 className="text-[62px] leading-none text-black1 font-medium font-roboto">Pellentesque suscipit<br/>fringilla libero eu.</h1>
           <button className="rounded-md bg-green1 mt-7  text-white cursor-pointer border-2 flex justify-center items-center text-[15px] tracking-wide  py-3.5 px-7 group">Get a Demo  <img src="Right1.png" alt="" className="group-hover:translate-x-2 ml-2" /></button>

        </div>
        )
    }
    export default Frame16
