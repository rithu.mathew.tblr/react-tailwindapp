import React from "react";

const Hamburger = ({ isOpen, toggle }) => {
  return (
    <button
      onClick={toggle}
      className={`block md:hidden w-8 h-8`}
    >
      <svg viewBox="0 0 20 20" fill="currentColor" className="w-full h-full">
        <path
          fillRule="evenodd"
          className={`${isOpen ? 'hidden' : 'block'}`}
          d="M4 6h12v1H4zm0 5h12v1H4zm0 5h12v1H4z"
          clipRule="evenodd"
        />
        <path
          fillRule="evenodd"
          className={`${isOpen ? 'block' : 'hidden'}`}
          d="M3 12h14a1 1 0 0 1 0 2H3a1 1 0 0 1 0-2zm0-5h14a1 1 0 0 1 0 2H3a1 1 0 0 1 0-2zm0 10h14a1 1 0 0 1 0 2H3a1 1 0 0 1 0-2z"
          clipRule="evenodd"
        />
      </svg>
    </button>
  );
};

export default Hamburger;
