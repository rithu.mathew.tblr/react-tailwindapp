import React, { useEffect } from "react";

const Sidebar = ({ isOpen, onClose }) => {
  useEffect(() => {

    if (isOpen) {
      document.body.classList.add("overflow-hidden");
    } else {
      document.body.classList.remove("overflow-hidden");
    }


    return () => {
      document.body.classList.remove("overflow-hidden");
    };
  }, [isOpen]);

  return (
    <div
      className={`fixed md:hidden -left-3 inset-0 w-full z-10 transform ${isOpen ? "translate-x-0" : "-translate-x-full"
        } transition duration-300 ease-in-out`}
      onClick={onClose}
    >
      <div
        className="absolute inset-y-0 top-[64px] w-full bg-gray shadow-lg"
        onClick={(e) => e.stopPropagation()}
      >
        <div className="flex flex-col justify-between space-y-7 px-12 py-9 sm:px-16 sm:py-12 list-none text-lg font-normal not-italic font-sans1 text-gray-900">
          <li>
            <a className="hover:text-blue-400" href="s">
              Home
            </a>
          </li>
          <li>
            <a className="hover:text-blue-400" href="s">
              Service
            </a>{" "}
          </li>
          <li>
            <a className="hover:text-blue-400" href="s">
              Feature
            </a>{" "}
          </li>
          <li>
            <a className="hover:text-blue-400" href="s">
              Product
            </a>{" "}
          </li>
          <li>
            <a className="hover:text-blue-400" href="s">
              Testimonial
            </a>{" "}
          </li>
          <li>
            <a className="hover:text-blue-400" href="s">
              FAQ
            </a>{" "}
          </li>
        </div>
      </div>
    </div>
  );
};

export default Sidebar;

