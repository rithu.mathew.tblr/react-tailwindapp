function Customers() {
    return (
        <div className="flex flex-col lg:flex-row bg-gray mt-8 justify-center items-center px-5 space-y-10 lg:px-24  py-5 lg:space-x-16 ">
            <div className="w-80   h-80 cursor-pointer" >
                <img src="image 9.png"  alt="" />
            </div>
            
            <div className=" flex  flex-col items-start mt-2  ">
               
                <p className="text-gray3 text-[14px] font-sans1 tracking-normal font-medium ">Maecenas dignissim justo eget nulla rutrum molestie. Maecenas lobortis sem dui, vel rutrum risus<br/>tincidunt ullamcorper. Proin eu enim metus. Vivamus sed libero ornare, tristique quam in, gravida<br/> enim. Nullam ut molestie arcu, at hendrerit elit. Morbi laoreet elit at ligula molestie, nec molestie<br/> mi blandit. Suspendisse cursus tellus sed augue ultrices, quis tristique nulla sodales. Suspendisse <br/> eget lorem eu turpis vestibulum pretium. Suspendisse potenti. Quisque malesuada enim sapien,<br/> vitae placerat ante feugiat eget. Quisque vulputate odio neque, eget efficitur libero condimentum<br/> id. Curabitur id nibh id sem dignissim finibus ac sit amet magna.
                </p>
                 <h4 className="text-green1 font-semibold  text-lg mt-4">Tim Smith</h4>
                 <p className="text-gray4 text-[15px] mt-2 hover:text-black hover:underline cursor-pointer">British Dragon Boat Racing Association</p>
                 <div className="flex mt-4 flex-col  " >
         <div className="flex flex-col xl:flex-row  space-y-3 xl:space-x-7 mt-4 xl:space-y-0 ">
            <div className="flex flex-wrap lg:flex-row space-x-9 lg:justify-between ">
            <img src="custLogo1.png" alt=""  width="45" height="37"/>
        <img src="custLogo2.png" alt="" width="45" height="37"/>
        <img src="custLogo5.png" alt="" width="45" height="37" />
        <img src="custLogo6.png" alt="" width="45" height="37"/>
        <img src="custLogo3.png" alt="" width="45" height="37" />
        <img src="custLogo4.png" alt="" width="45" height="37"/>
       
            </div>

   
        <div className="flex  items-center justify-center group    ">
            <p className="text-lg text-green1 font-sans1 font-semibold mr-2 group-hover:underline  cursor-pointer">Meet all customers</p>
            <img src="Right.png" className="group-hover:translate-x-2" alt="" />
        </div>
        </div>
        </div>
            </div>
        </div>
     );
}
export default Customers