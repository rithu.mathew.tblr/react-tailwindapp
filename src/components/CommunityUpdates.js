function Community() {
    return (
        <div className="flex flex-col px-20 lg:px-28 space-y-3 mt-9">
            <div className="flex flex-col items-center text-center">
                <h2 className="text-[32px]  font-semibold font-sans1 leading-tight   text-gray1 ">Caring is the new marketing</h2>

                <p className="text-gray2 text-sm mt-2 leading-relaxed tracking-normal font-sans1">The Nexcent blog is the best place to read about the latest membership insights,<br />trends and more. See who's joining the community, read about how our community<br />are increasing their membership income and lot's more.​</p>
            </div>
            <div className="flex flex-col  lg:justify-center space-y-20 lg:flex-row sm:space-y-32 md:space-y-28 lg:space-y-0 lg:space-x-4 justify-between items-center">
                <div className="relative transform transition-transform hover:scale-105 cursor-pointer group ">
                    <img src="image 18.png" className="rounded-md w-full object-fill " alt="" />
                    <div className="group-hover:bg-green-100 sm:absolute h-[130px] sm:h-[167px] lg:h-[220px] xl:h-[167px] space-y-5 bg-gray5 w-full sm:w-[90%] sm:left-4 sm:-bottom-20 lg:top-32 shadow-bottom1 xl:top-48 justify-between rounded-md p-4 text-center">
                        <h3 className="group-hover:text-black text-gray3 font-sans1 font-semibold text-[12px] sm:text-[19px]" >Creating Streamlined<br />Safeguarding Processes<br />with OneRen</h3>
                        <div className="flex items-center justify-center group">
                            <p className="text-[14px] sm:text-[18px] group-hover:underline text-green1 font-sans1 font-semibold mr-2 ">Readmore</p>
                            <img src="Right.png" alt="" className="group-hover:translate-x-2" />
                        </div>
                    </div>

                </div>
                <div className="relative transform transition-transform hover:scale-105 cursor-pointer group">
                    <img src="image 19.png" className="relative rounded-md w-full object-fill " alt="" />
                    <div className="group-hover:bg-green-100 sm:absolute h-[130px] sm:h-[167px] lg:h-[220px] xl:h-[167px] space-y-5  bg-gray5 w-full sm:w-[90%] sm:left-4 sm:-bottom-20 shadow-bottom1 lg:top-32  xl:top-48 justify-between rounded-md p-4 text-center">
                        <h3 className="group-hover:text-black  text-gray3 font-sans1 font-semibold text-[12px] sm:text-[19px] ">What are your safeguarding<br />responsibilities and how can<br />you manage them?</h3>
                        <div className="flex items-center  justify-center group ">
                            <p className="text-[14px] sm:text-[18px]  group-hover:underline text-green1 font-sans1 font-semibold mr-2  ">Readmore</p>
                            <img src="Right.png" alt="" className="group-hover:translate-x-2" />
                        </div>
                    </div>
                </div>
                <div className="relative transform transition-transform hover:scale-105 cursor-pointer group">
                    <img src="image 20.png" className="relative  rounded-md  w-full object-fill" alt="" />
                    <div className="group-hover:bg-green-100 sm:absolute h-[130px] sm:h-[167px] lg:h-[220px] xl:h-[167px]  space-y-5 bg-gray5 w-full sm:w-[90%] sm:left-4 sm:-bottom-20 shadow-bottom1 lg:top-32  xl:top-48 justify-between rounded-md p-4 text-center">
                        <h3 className="group-hover:text-black text-gray3 font-sans1 font-semibold text-[12px] sm:text-[19px]  ">Revamping the Membership<br />Model with Triathlon<br />Australia</h3>
                        <div className="flex items-center  justify-center">
                            <p className="text-[14px] sm:text-[18px] group-hover:underline text-green1 font-sans1 font-semibold mr-2 ">Readmore</p>
                            <img src="Right.png" alt="" className="group-hover:translate-x-2" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Community

