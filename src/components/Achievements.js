import React, { useState } from 'react';
import CountUp from 'react-countup';
import ScrollTrigger from 'react-scroll-trigger';
function Achievements() {
    
        const [count, setCount] = useState(false);

  
    return (
        <div className="flex flex-col md:flex-row bg-gray items-center  justify-between space-y-8 md:space-y-0 md:space-x-3 lg:px-16 lg:space-x-0 xl:px-28  px-[130px] md:px-9 mt-8 py-[56px]  ">
            <div className="flex flex-col md:space-y-2">
                <h2 className="text-[33px]  font-semibold font-sans1  leading-none     text-gray1 ">Helping a local</h2>
                <h2 className="text-[33px]  font-semibold font-sans1 leading-none      text-green1">business reinvent itself</h2>
                <p className="text-black  text-[15px] tracking-wide mt-3.5   ">We reached here with our hard work and dedication
                </p>
            </div>
            <div className="flex flex-col  space-y-7  ">
                <div className="flex flex-col md:flex-row md:space-y-0 space-y-8    ">
                    <div className="flex  items-center   px-10 space-x-2   w-64 md:w-54 ">
                        <img src="achieve4.png" className="w-11 h-11" alt="" />
                        <div className="text-left flex  flex-col   ">
                        <ScrollTrigger  onEnter={()=>setCount(true)} onExit={()=>setCount(false)}>
                            <h2 className="text-[25px] font-bold font-sans1  text-gray1">{count && < CountUp start={0} end={2245341} duration={2} delay={0} />}</h2>
                            </ScrollTrigger>
                            <p className="text-sm text-gray2 leading-none tracking-wide">Members</p>
                        </div>
                    </div>
                    <div className="flex  items-center px-10 space-x-2 w-64 md:w-54  ">
                        <img src="achieve3.png" alt="" className="w-11 h-11" />
                        <div className="  text-left flex   flex-col ">
                            <ScrollTrigger  onEnter={()=>setCount(true)} onExit={()=>setCount(false)}>
                            <h2 className="text-[25px] font-bold font-sans1  text-gray1">{count && < CountUp start={0} end={46325} duration={2} delay={0} />}</h2>
                            </ScrollTrigger>
                            
                            <p className="text-sm text-gray2 tracking-wide">Clubs</p>
                        </div>
                    </div>
                </div>

                <div className="flex   flex-col  space-y-7  md:flex-row md:space-y-0 ">
                    <div className="flex items-center  px-11 space-x-2 w-64 md:w-54  ">
                        <img src="achieve2.png" className="w-11 h-11" alt="" />
                        <div className="text-left flex   flex-col  ">
                        <ScrollTrigger  onEnter={()=>setCount(true)} onExit={()=>setCount(false)}>
                            <h2 className="text-[25px] font-bold font-sans1  text-gray1">{count && < CountUp start={0} end={828867} duration={2} delay={0} />}</h2>
                            </ScrollTrigger>
                            
                            <p className="text-sm text-gray2 tracking-wide">Event Bookings</p>
                        </div>
                    </div> 
                    <div className="flex  items-center px-10 space-x-2 w-64 md:w-54  "><img src="achieve1.png" className="w-11 h-11" alt="" />
                        <div className="text-left flex flex-col   ">
                        <ScrollTrigger  onEnter={()=>setCount(true)} onExit={()=>setCount(false)}>
                            <h2 className="text-[25px] font-bold font-sans1  text-gray1">{count && < CountUp start={0} end={1926436} duration={2} delay={0} />}</h2>
                            </ScrollTrigger>
                           
                            <p className="text-sm text-gray2 tracking-wide">Payments</p>
                        </div></div>

                </div>
            </div>
        </div>


    );
}
export default Achievements