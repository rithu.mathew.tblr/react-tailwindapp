import React, { useState } from "react";
import Hamburger from "./hamburger";
import Sidebar from "./sidebar";
import { useAuth0 } from "@auth0/auth0-react";


function Nav() {

  const [isOpen, setIsOpen] = useState(false);

  const { loginWithRedirect, isAuthenticated, logout, user } = useAuth0();

  const toggleMenu = () => {
    setIsOpen(!isOpen);
  };
  const closeMenu = () => {
    setIsOpen(false);
  };
  return (
    <div className="px-7 py-5 bg-gray space-x-7 flex justify-between items-center sm:px-12 lg:px-16 xl:px-20">
      <div className="flex justify-between space-x-3 ml-0 items-center">
        <Hamburger isOpen={isOpen} toggle={toggleMenu} />
        <div className="w-32 xl:w-36  ">
          <img src="Logo.png" alt="" />
        </div>
        <Sidebar isOpen={isOpen} onClose={closeMenu} />

      </div>
      <div className=" hidden   md:flex min-[860px]:space-x-6 lg:space-x-10 xl:space-x-12 text-sm space-x-3 list-none text-[13px] font-normal not-italic font-sans1 text-gray-900   ">

        <li><a href="s" className=" hover:underline">Home</a></li>
        <li><a href="s" className=" hover:underline">Service</a> </li>
        <li><a href="s" className=" hover:underline">Feature</a> </li>
        <li><a href="s" className=" hover:underline">Product</a> </li>
        <li><a href="s" className=" hover:underline">Testimonial</a> </li>
        <li><a href="s" className=" hover:underline">FAQ</a> </li>

      </div>
      <div className="flex justify-between items-center space-x-3 sm:space-x-5 ">

        {!isAuthenticated ?
          <button onClick={() => loginWithRedirect()} className="text-green1  text-sm font-normal xl:text-[13px] focus:outline-none hover:underline">Login</button>
          :
          (
            <>
              <p className="text-xs">Hello,{(user.name)}..</p>
              <button onClick={() => logout()} className="bg-green1 text-white cursor-pointer flex justify-center font-sans1 hover:bg-green-500 items-center rounded text-xs px-2 py-2 sm:px-4 xl:text-[13px]">Sign Out</button>
            </>
          )}

      </div>
    </div>
  )
}
export default Nav;
