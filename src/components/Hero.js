import React from "react";
import Slider from "react-slick";

function Hero() {

  var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };
  return (
    
    <Slider  {...settings}>

      <div>
        <div className="flex flex-col bg-gray py-12 space-y-7 sm:space-x-9 md:space-x-11 lg:space-x-12 items-center  px-20 sm:py-14 md:py-16 xl:flex-row xl:px-32  xl:justify-between">

          <div className=" flex flex-col items-start  space-y-4">
            <h1 className="text-[54px] sm:text-[56px] md:text-[59px] lg:text-[70px] xl:text-[62px] leading-none text-gray1 font-medium font-roboto">Lessons and insights</h1>
            <h1 className="text-[54px]  sm:text-[56px] md:text-[59px] lg:text-[70px] xl:text-[62px]   leading-none text-green1 font-medium font-roboto"> from 8 years</h1>
            <p className="text-gray2 text-sm tracking-wider mt-3">Where to grow your business as a photographer:site or social media?</p>
            <button className="rounded-md bg-green1 mt-4  hover:bg-green-500  text-white cursor-pointer border-2 flex justify-center items-center text-sm tracking-wide  py-3 px-7 ">Register</button>
          </div>

          <div className="h-64 w-64 sm:w-72 sm:h-80 md:w-80 md:h-80 lg:w-96   pl-4 xl:pl-0 xl:h-[360px] md:pl-7 ">
            <img src="Illustration.png" alt="" />
          </div>

        </div>
      </div>

      <div>

        <div className="flex flex-col bg-gray py-12 space-y-7 sm:space-x-9 md:space-x-11 lg:space-x-12 items-center  px-20 sm:py-14 md:py-16 xl:flex-row xl:px-32  xl:justify-between">


          <div className=" flex flex-col items-start  space-y-4  ">
            <h1 className="text-[54px] sm:text-[56px] md:text-[59px] lg:text-[70px] xl:text-[62px] leading-none text-gray1 font-medium font-roboto">Lessons and insights</h1>
            <h1 className="text-[54px]  sm:text-[56px] md:text-[59px] lg:text-[70px] xl:text-[62px]   leading-none text-green1 font-medium font-roboto"> from 8 years</h1>
            <p className="text-gray2 text-sm tracking-wider mt-3">Where to grow your business as a photographer:site or social media?</p>
            <button className="rounded-md bg-green1 mt-4  hover:bg-green-500  text-white cursor-pointer border-2 flex justify-center items-center text-sm tracking-wide  py-3 px-7 ">Register</button>




          </div>
          <div className="h-64 w-64 sm:w-72 sm:h-80 md:w-80 md:h-80 lg:w-96  pl-4 xl:pl-0  xl:h-[360px] md:pl-7   ">
            <img src="illu1.svg" alt="" className="h-full   " />
          </div>


        </div> 

      </div>

      <div>

        <div className="flex flex-col bg-gray py-12 space-y-7 sm:space-x-9 md:space-x-11 lg:space-x-12 items-center  px-20 sm:py-14 md:py-16 xl:flex-row xl:px-32  xl:justify-between">


          <div className=" flex flex-col items-start  space-y-4  ">
            <h1 className="text-[54px] sm:text-[56px] md:text-[59px] lg:text-[70px] xl:text-[62px] leading-none text-gray1 font-medium font-roboto">Lessons and insights</h1>
            <h1 className="text-[54px]  sm:text-[56px] md:text-[59px] lg:text-[70px] xl:text-[62px]   leading-none text-green1 font-medium font-roboto"> from 8 years</h1>
            <p className="text-gray2 text-sm tracking-wider mt-3">Where to grow your business as a photographer:site or social media?</p>
            <button className="rounded-md bg-green1 mt-4  hover:bg-green-500  text-white cursor-pointer border-2 flex justify-center items-center text-sm tracking-wide  py-3 px-7 ">Register</button>




          </div>
          <div className="h-64 w-64 sm:w-72 sm:h-80 md:w-80 md:h-80 lg:w-96   pl-4 xl:pl-0 xl:h-[360px] md:pl-7  ">
            <img src="illu2.svg" alt="" className="h-full   " />
          </div>


        </div>

      </div>
    </Slider>



  );
}
export default Hero