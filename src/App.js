import './App.css';
import Nav from './components/Nav';
import Hero from './components/Hero';
import Client from './components/Clients';
import Community from './components/Community';
import Unlock from './components/Unlock';
import Achievements from './components/Achievements';
import Calender from './components/Calender';
import Customers from './components/Customers';
import CommunityUpdates from './components/CommunityUpdates'
import Frame16 from './components/Frame16';
import Footer from './components/Footer';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./assets/css/_custom.css"

function App() {
 
  return (
    <>
      <Nav />
      <Hero />
      <Client />
      <Community />
      <Unlock />
    <Achievements/>
    <Calender/>
    <Customers/>
   <CommunityUpdates/>
   <Frame16/>
   <Footer/>
    </>
  );
}

export default App;
